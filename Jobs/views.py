from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from . import serializers
from . import models
from .tasks import schedule_job
from rest_framework.decorators import api_view
import json


@api_view(['GET', 'POST'])
def jobs(request):
  if request.method == 'POST':
    job = models.Job()
    job.job_description = json.dumps(request.data)
    job.save()
    schedule_job.apply_async((job.job_id, ))
    return Response(serializers.JobListSerializer(job).data)
  else:
    jobs = models.Job.objects.all()
    serializer = serializers.JobListSerializer(jobs, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def get_job(request, pk):
  job = models.Job.objects.get(pk=pk)
  serializer = serializers.JobSerializer(job)
  return Response(serializer.data)
