from .models import Job, JobStatus
from Api import celery_app
from contextlib import contextmanager
import json
import Analytics.engine
import Analytics.compiler
import time

_engine = None


@contextmanager
def timer(format_string):
  start = time.time()
  yield
  elapsed = time.time() - start
  print(format_string % elapsed)


def date_handler(obj):
    if hasattr(obj, 'isoformat'):
        return obj.isoformat()
    else:
        raise TypeError


def prepare_engine(*args, **kwargs):
  """ This is an ugly hack to share the object accross tasks.
  We can do this since we will only ever run tasks one at a time.
  """
  global _engine
  if _engine is None:
    _engine = Analytics.engine.AnalyticsEngine()
  return _engine


@celery_app.task
def schedule_job(job_id):
  with timer('Task completed in %sms.'):
    print("Starting executing job for %s." % job_id)
    job = Job.objects.get(pk=job_id)
    print(job.job_description)
    job.status = JobStatus.RUNNING
    job.save()
    try:
      data = json.loads(job.job_description)
      analytics_job = Analytics.compiler.Job(data['metrics'], data['dimensions'])
      engine = prepare_engine()
      result = engine.run_job(analytics_job)
      job.status = JobStatus.FINISHED
      job.result = json.dumps(result, default=date_handler)
      job.save()
    except Exception as e:
      job.status = JobStatus.ERROR
      job.result = str(e)
      print(str(e))
      job.save()
      raise e
