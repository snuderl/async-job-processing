from rest_framework import serializers
from Jobs.models import Job


class SubmitJobSerializer(serializers.Serializer):
  metrics = serializers.ListField(serializers.CharField())
  dimensions = serializers.ListField(serializers.CharField())


class JobListSerializer(serializers.ModelSerializer):
  class Meta:
    model = Job
    exclude = ('result', )


class JobSerializer(serializers.ModelSerializer):
  class Meta:
    model = Job
    fields = '__all__'
