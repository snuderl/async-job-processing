from django.db import models
import uuid


class JobStatus(object):
  SUBMITED = "SUBMITED"
  RUNNING = "RUNNING"
  ERROR = "ERROR"
  FINISHED = "FINISHED"


# Create your models here.
class Job(models.Model):
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  job_id = models.UUIDField(primary_key=True, default=uuid.uuid4)
  status = models.CharField(default=JobStatus.SUBMITED, max_length=10)

  job_description = models.TextField()
  result = models.TextField(null=True)
