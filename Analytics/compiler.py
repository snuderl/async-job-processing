from sqlbuilder.smartsql import Q, T, func
from sqlbuilder.smartsql.dialects.mysql import compile

class Job(object):
  def __init__(self, metrics, dimensions):
    self.metrics = metrics
    self.dimensions = dimensions

  def to_sql(self, table_name):
    return compile_to_sql(self.metrics, self.dimensions, table_name)


def compile_to_sql(metrics, dimensions, table_name):
  table = getattr(T, table_name)

  fields = []
  for metric in metrics:
    field = getattr(table, metric)
    fields.append(func.avg(field).as_('avg_%s' % metric))
    fields.append(func.count(field).as_('count_%s' % metric))
  group_by = [getattr(T, dimension) for dimension in dimensions]
  fields.extend(group_by)

  query = Q()\
      .tables(table)\
      .fields(*fields)\
      .group_by(*group_by)
  return compile(query)[0]
