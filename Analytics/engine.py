from pyspark.sql import SparkSession


DB_PATH = 'data_store'


def get_spark():
  spark = SparkSession.builder.getOrCreate()
  return spark


class AnalyticsEngine(object):
  def __init__(self, path=DB_PATH):
    self.spark = get_spark()
    self.db_path = path
    self.df = self._load_df()

    # We need to register the dataframe as a table to be able to run sql queries against it
    self.table_name = 'db'
    self.df.createOrReplaceTempView(self.table_name)

  def _load_df(self):
    return self.spark.read.parquet(self.db_path)

  def run_job(self, job):
    sql = job.to_sql(self.table_name)
    print('Executing %s' % sql)
    df = self.spark.sql(sql)
    print(df.explain())
    return df.collect()
