import random
import radar
from collections import namedtuple

metrics = ['M1', 'M2', 'M3', 'M4', 'M5']


accounts = list(range(10))
campaigns = {}

campaign_id = 0
for account in accounts:
  for i in range(account):
    campaigns[campaign_id] = account
    campaign_id += 1


creatives = {}
for i in range(1000):
  creatives[i] = random.randint(0, campaign_id - 1)

placements = {}
for i in range(1000):
  placements[i] = random.randint(0, campaign_id - 1)


Row = namedtuple('Row', 'datetime date M1 M2 M3 M4 M5 account campaign placement creative')


def generate_data(num):
  data = []
  for i in range(num):
    if i % 2 == 0:
      placement = random.randint(0, len(placements) - 1)
      campaign = placements[placement]
      creative = None
    else:
      placement = None
      creative = random.randint(0, len(creatives) - 1)
      campaign = creatives[creative]

    account = campaigns[campaign]
    datetime = radar.random_datetime(start='2017-01-01T00:00:00', stop='2017-02-28T23:59:59')

    row = Row(
      datetime,
      datetime.date(),
      random.randint(0, 10**4),
      random.randint(0, 10**5),
      random.randint(0, 10**6),
      random.randint(0, 10**7),
      random.randint(0, 10**8),
      account,
      campaign,
      placement,
      creative,
    )
    data.append(row)
  return data


def create_random_data(spark, n_rows, location, mode='append'):
  schema = spark.createDataFrame(generate_data(10)).schema
  spark.createDataFrame(generate_data(n_rows), schema=schema)\
       .write.partitionBy('date')\
             .mode(mode)\
             .parquet('data_store')


if __name__ == '__main__':
  import pyspark.sql
  spark = pyspark.sql.SparkSession.builder.getOrCreate()
  create_random_data(spark, 10**6, 'data_store')
