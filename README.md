This is a simple proof of concept implementation of asynchronus task runner.

### Architecture

* Simple api that allows submiitng jobs. Each job gets stored to a database, and added to a queue
* Workers that pull tasks of the queue, run them and update database records with result
* Analytics DB/storage is a local file system at the moment but that is meant to be moved to HDFS/S3.


### Api
Not much to say about this, it supports 3 simple endpoints. List, get and put.
Task results are stored directly in the db at the moment, but since they can get pretty large pretty quickly they could be moved to a separate storage like s3.

### Workers
Worker queue is managed by celery which is not really used much here except to give us a nice way to start, and queue tasks. Database used to store the queue here is redis.

For actual analytics calculation spark is used. Our idea is for each worker to have its own local spark instance that is then used to execute query against a file like database.
The way we run queries at the moment is that we compile the task specification to an sql statement that we then feed to spark (this for example gives us some leeway if ever a switch from spark to something else SQL-like is needed).

### Data Storage
Actual data store is just a bunch of files. In our example files are partitioned by date, which would allow for efficient filtering on dates.
All the data rows are completely denormalized. We do this since we expect raw data storage to be cheap, while giving us better read performance when grouping and filtering on eg. accounts or campaigns.
Actual on disk data format used is parquet, which is a columnar format allowing us to work with only a subset of fields.


### Some thoughts
* Using spark was just an idea I had that I wanted to try out here. Spark has some overhead so we can never expect sub second query execution.
* Scalability here can be achieved in two ways (more workers to be able to run more tasks in parallel, or bigger workers if tasks take too long (potentially even clusters)).
* Using s3 as a database imposes some constraints:
  * No good indexing support. This can be worked around with partitioning and possibly data formats such as ORC (has some internal indexing afaik)
  * We don't want too many small files so we might need to buffer the writtes to the database until some threshold is reached.


### Runing the code
This was tested with python3.5 and spark2.1.

Once python and spark (SPARK_HOME, PYTHONPATH) are setup and redis is running localy.

```bash
  virtualenv -p python3 .venv
  source .venv/bin/activate
  pip install -r requirements.txt
  python manage.py migrate
  
  # To run webserver
  python manage.py runserver
  # To run worker instance
  celery -A Api worker -E --concurrency=1  -P solo
```






